# Marriage and Divorce in Iran

**https://www.kaggle.com/code/leilasaffarian/marriage-and-divorce-in-iran**

About Dataset  
The number of Iranians getting divorced are on the rise, according to the latest data from the country's Statistics Center.

While the number of marriages in Iran, too, increased during the period between 2019 and 2020 by 4.4%, reaching 556,731 cases, the divorce rates rose by 3.6%, reaching 183,193 cases.

This means that in 2020, for every 100 registered marriages, 32.9 divorces occurred.

According to the statistics reported in Iran's Report on the Social and Cultural Status, published in the fall of 2020, some 51,270 marriages that ended in divorce lasted between one to five years.

Furthermore, 11,715 marriages ended in divorce in just one year. On the other hand, 7,809 marriages of around 29 years were terminated.

In the 2020 report, neither a complete national nor a granular provincial marriage and divorce data has been published therefore data on this topic had to be sourced from other reports.

The latest statistical report on Iran’s divorce and marriage, categorized by province, is related to the first half of last year. This information was taken from the Civil Registration Organization of Iran’s website. This article analyzes this data.

The highest number of marriages, in the first half of 2020, happened in Tehran. During this period, 34,451 marriages were recorded in this province while the lowest number of marriages, namely 1,914 marriages, was recorded in the same period in Semnan province.

The highest number of divorces, in the first half of 2020, was recorded in the Tehran province. During this period, 15,303 divorces were recorded.

Ilam province has the lowest number of divorces, in the same time period, which was 388 cases.

The numerical quantity of marriage and divorce cases is not a good basis for comparison because of the population differences between provinces. Accurate and proportional comparison requires careful consideration of the province’s population that are being compared.

Adding the population variable to this data changes some of the provincial marriage and divorce rankings.

Statistical source:

https://www.amar.org.ir/%D9%BE%D8%A7%DB%8C%DA%AF%D8%A7%D9%87-%D9%87%D8%A7-%D9%88-%D8%B3%D8%A7%D9%85%D8%A7%D9%86%D9%87-%D9%87%D8%A7/%D8%B3%D8%B1%DB%8C%D9%87%D8%A7%DB%8C-%D8%B2%D9%85%D8%A7%D9%86%DB%8C/agentType/ViewType/PropertyTypeID/1938


The number of Iranians getting divorced are on the rise, according to the latest data from the country's Statistics Center. While the number of marriages in Iran, too, increased during the period between 2019 and 2020 by 4.4%, reaching 556,731 cases, the divorce rates rose by 3.6%, reaching 183,193 cases.
This means that in 2020, for every 100 registered marriages, 32.9 divorces occurred.


